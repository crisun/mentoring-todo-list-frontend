import axios from 'axios';

export const fetchTodos = async () => {
  const response = await axios.get('http://localhost:8080/todos');
  return response.data;
}

export const createTodo = async (todo) => {
  const response = await axios.post('http://localhost:8080/todo', todo);
  return response.data;
}

export const updateTodo = async (id, todo) => {
  const response = await axios.put(`http://localhost:8080/todo/${id}`, todo)
  return response.data;
}

export const deleteTodo = async (id) => {
  const response = await axios.delete(`http://localhost:8080/todo/${id}`)
  return response.data;
}