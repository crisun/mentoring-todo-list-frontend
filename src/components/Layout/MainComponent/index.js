import React from 'react';

const MainComponent = ({children}) => {
  return (
    <div className="App">
      <h2>My Todo List</h2>
      {children}
    </div>
  )
}

export default MainComponent;