import React from 'react';
import PropTypes from 'prop-types';

const List = ({ items, ItemComponent, onItemClick, onItemDelete }) => {
  return (
    <ul>
      {
        items.map(item => <ItemComponent key={item.id} {...item} onItemClick={onItemClick} onItemDelete={onItemDelete}/>)
      }
    </ul>
  )
}

List.propTypes = {
  items: PropTypes.array.isRequired,
  ItemComponent: PropTypes.elementType.isRequired,
  onItemClick: PropTypes.func,
  onItemDelete: PropTypes.func
}

export default List;