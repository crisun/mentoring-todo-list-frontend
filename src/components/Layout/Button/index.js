import React from 'react';
import './index.css';

const Button = ({ type = "main", onClick, label }) => <button className={`btn btn-${type}`} onClick={onClick}>{label}</button>;

export default Button;

