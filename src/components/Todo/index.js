import React, { Component } from 'react';
import TodoForm from './TodoForm';
import TodoItem from './TodoItem';
import List from '../Layout/List';
import Modal from '../Layout/Modal';
import Loader from '../Layout/Loader';
import MainComponent from '../Layout/MainComponent';
import { fetchTodos, createTodo, updateTodo, deleteTodo } from '../../api';
import { statusEnum } from '../../enums/status';

class Todo extends Component {
  state = {
    todoList: [],
    currentTodo: {
      id: 1,
      title: "",
      description: "",
      status: statusEnum.CREATED
    },
    error: {
      title: "",
      message: ""
    },
    isLoading: false
  }

  componentDidMount = async () => {
    this.setState({
      isLoading: true
    });

    const response = await fetchTodos();

    this.setState({
      todoList: response,
      isLoading: false
    })    
  }

  handleChange = event => {
    this.setState({
      currentTodo: {
        ...this.state.currentTodo,
        id: this.state.todoList.length + 1,
        [event.target.name]: event.target.value
      }
    })
  }

  handleSubmit = async event => {
    event.preventDefault(); 
    await this.setState({ isLoading: true }); 
    const { currentTodo } = this.state; 

    if (currentTodo.title) {
      const { todoList } = this.state; 
      const todo = await createTodo(currentTodo); 

      todoList.push(todo);


      this.setState({
        todoList: todoList,
        currentTodo: {
          title: "",
          description: ""
        },
        isLoading: false
      })
    } else {
      this.setState({
        error: {
          title: "Erro ao adicionar tarefa",
          message: "Você deve preencher pelo menos o título"
        },
        isLoading: false
      })
    }
  }

  handleUpdate = async id => {
    await this.setState({ isLoading: true });

    const newStateTasks = this.state.todoList.map(task => {
      if (task.id === id) {
        const data = {
          ...task,
          status: task.status === statusEnum.FINISHED ? statusEnum.STARTED : statusEnum.FINISHED
        }
        updateTodo(data.id, data)
        return data;
      }

      return task;
    });

    this.setState({
      todoList: newStateTasks,
      isLoading: false
    });
  }

  handleDelete = async id => {
    await this.setState({ isLoading: true });
    await deleteTodo(id);

    const remainingTasks = this.state.todoList.filter(task => task.id !== id);

    this.setState({
      todoList: remainingTasks,
      isLoading: false
    });
  }

  handleError = () => {
    this.setState({
      error: {
        title: "",
        message: ""
      }
    })
  }

  render() {
    return (
      <MainComponent>
        <TodoForm 
          onFieldChange={this.handleChange} 
          onSubmit={this.handleSubmit} 
          title={this.state.currentTodo.title} 
          description={this.state.currentTodo.description}
        />
        <List 
          items={this.state.todoList}
          ItemComponent={TodoItem}
          onItemClick={this.handleUpdate}
          onItemDelete={this.handleDelete}
        />
        <Modal 
          title={this.state.error.title}
          message={this.state.error.message}
          onClose={this.handleError}
          onOpen={this.state.error.title !== ""}
        />
        <Loader isLoading={this.state.isLoading}/>
      </MainComponent>
    )
  }
}

export default Todo;