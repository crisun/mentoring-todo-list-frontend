import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import Todo from '../Todo';
import TodoForm from '../Todo/TodoForm';

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Todo} />
        <Route exact path="/edit/:id" component={TodoForm} />
      </Switch>
    </Router>
  )
}

export default Routes