## Todo List

### Como rodar o projeto?

- É necessário ter o backend rodando na `localhost:8080` para que o projeto funcione: `https://bitbucket.org/crisun/mentoring-todo-list-bff/src/master/`
- Instalando as dependências do package.json:  `npm install`
- Rodando o projeto: `npm start`