import React from 'react';
import Button from '../../Layout/Button';
import './index.css';
import { statusEnum } from '../../../enums/status';

const TodoItem = ({ id, title, description, status, onItemClick, onItemDelete }) => {
  return (
    <li key={id} className="list__item">
      <div onClick={() => onItemClick(id)}>
        <p className={`list__title       ${status === statusEnum.FINISHED ? 'list__title--striked' : ''}`}>{title}</p>
        <p className={`list__description ${status === statusEnum.FINISHED ? 'list__description--striked' : ''}`}>{description}</p>
      </div>
      <Button type="delete" onClick={() => onItemDelete(id)} label="Deletar"/>
    </li>
  )
}

export default TodoItem;