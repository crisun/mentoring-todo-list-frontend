import React from 'react';
import Button from '../Button';
import './index.css';

const Modal = ({title, message, onOpen, onClose}) => {
  return (
    <div className={`modal ${onOpen ? 'modal-show': ''}`}>
      <div className="modal__content">
        <div className="modal__header">
          <p className="modal__title">{title}</p>
        </div>
        <p className="modal__description">{message}</p>
        <Button onClick={onClose} label="Fechar" />
      </div>
    </div>
  )
}

export default Modal;

