import React from 'react';
import './index.css';

const Loader = ({ isLoading }) => {
  return (
    isLoading && 
    <div className="loader">
      <div className="lds-hourglass"></div>
    </div>
  )
}

export default Loader;