# develop stage
FROM node:alpine as develop-stage
WORKDIR /app
RUN npm install -g create-react-app create-react-native-app react-native-cli

# build stage
FROM develop-stage as build-stage
RUN npm run build

# production stage
FROM nginx:alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
