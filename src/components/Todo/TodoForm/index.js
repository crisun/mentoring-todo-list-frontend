import React from 'react';
import Button from '../../Layout/Button';
import MainComponent from '../../Layout/MainComponent';
import './index.css';

const TodoForm = ({ onFieldChange, onSubmit, title, description }) => {
  return (
    document.location.pathname.split('/')[1] === "edit" ? 
    <MainComponent>
      <form className="form__container">
        <input className="form__input" type="text" onChange={onFieldChange} name="title" placeholder="Título" value={title}/>
        <input className="form__input" type="text" onChange={onFieldChange} name="description" placeholder="Descrição" value={description}/>
        <Button onClick={onSubmit} label="Adicionar"/>
      </form>
    </MainComponent> :
    <form className="form__container">
      <input className="form__input" type="text" onChange={onFieldChange} name="title" placeholder="Título" value={title}/>
      <input className="form__input" type="text" onChange={onFieldChange} name="description" placeholder="Descrição" value={description}/>
      <Button onClick={onSubmit} label="Adicionar"/>
    </form>
  )
}

export default TodoForm;